import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {PokemonsService} from "../../services/pokemons.service";

@Component({
  selector: 'app-form-pokemon',
  templateUrl: './form-pokemon.component.html',
  styleUrls: ['./form-pokemon.component.scss']
})
export class FormPokemonComponent implements OnInit {
  pokemonDetail: any = {};
  constructor(public dialogRef: MatDialogRef<FormPokemonComponent>,
              private pokemonsService: PokemonsService,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    console.log(this.data);
    this.getPokemonDetails();
  }

  getPokemonDetails(){
    this.pokemonsService.getPokemonDetails(this.data).subscribe(data => {
      this.pokemonDetail = data;
      console.log(data);
    });
  }

  closeModal() {
    this.dialogRef.close();
  }
}
