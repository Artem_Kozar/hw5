import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AlbumsService} from "../../services/albums.service";

@Component({
  selector: 'app-form-album',
  templateUrl: './form-album.component.html',
  styleUrls: ['./form-album.component.scss']
})
export class FormAlbumComponent implements OnInit {

  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<any>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private formBuilder: FormBuilder,
              private albumsService: AlbumsService
  ) {
  }

  ngOnInit(): void {
    this.setForm();
    if (this.data){this.form.setValue(this.data.album)}
  }

  closeModal(): void {
    this.dialogRef.close();
  }

  saveAlbum(album): void {
    let data = {name: album.name,
      band: album.band,
      label: album.label.split(', '),
      genre: album.genre.split(', '),
      producer: album.producer.split(', ')};
    if (this.data && this.data.id) {
      this.albumsService.editData(this.data.id, data).then(res => {
        console.log(res);
      });
    } else {
      this.albumsService.addData(data).then(res => {
        console.log(res);
      })
    }
    this.dialogRef.close();
  }


  setForm() {
    this.form = this.formBuilder.group({
      name: '',
      band: '',
      label: '',
      genre: '',
      producer: ''
    });
  }

}
